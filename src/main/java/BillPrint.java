import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	private final HashMap<String, Play> plays;
	private final String customer;
	private final ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap<>();
		for (Play p: plays) { this.plays.put(p.getId(),p); }

		this.customer = customer;
		this.performances = performances;
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}


	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}

	private static class EnrichedPerformance extends Performance{
		public Play play;
		public int amount;
		public int volumeCredits;

		public EnrichedPerformance(String playID, int audience) {
			super(playID, audience);
		}

		public EnrichedPerformance(Performance aPerformance){
			super(aPerformance.getPlayID(), aPerformance.getAudience());
		}
	}

	private static class StatementData{
		public String customer;
		public ArrayList<EnrichedPerformance> performances;
		public int totalAmount;
		public int totalVolumeCredits;

		public StatementData(){
			this.performances = new ArrayList<>();
		}
	}

	public String statement(){
		StatementData data = new StatementData();
		data.customer = this.customer;
		this.performances.forEach(perf -> data.performances.add(enrichPerformance(perf)));
		data.totalAmount = totalAmount(data);
		data.totalVolumeCredits = totalVolumeCredits(data);
		return renderPlainText(data);
	}

	private EnrichedPerformance enrichPerformance(Performance aPerformance){
		EnrichedPerformance result = new EnrichedPerformance(aPerformance);
		result.play = playFor(result);
		result.amount = amountFor(result);
		result.volumeCredits = volumeCreditsFor(result);
		return result;
	}

	public String renderPlainText(StatementData data) {
		String result = "Statement for " + data.customer + "\n";
		for (EnrichedPerformance perf: data.performances) {
			// print line for this order
			result += "  " + perf.play.getName() + ": $" + usd(perf.amount) + " (" + perf.getAudience()
					+ " seats)" + "\n";
		}

		result += "Amount owed is $" + usd(data.totalAmount) + "\n";
		result += "You earned " + data.totalVolumeCredits + " credits" + "\n";
		return result;
	}

	private int totalAmount(StatementData data) {
		int result = 0;
		for (EnrichedPerformance perf: data.performances) {
			result += perf.amount;
		}
		return result;
	}

	private int totalVolumeCredits(StatementData data) {
		int volumeCredits = 0;
		for (EnrichedPerformance perf: data.performances){
			// add volume credits
			volumeCredits += perf.volumeCredits;
		}
		return volumeCredits;
	}

	private String usd(int aNumber) {
		return new DecimalFormat("#.00").format(aNumber / 100);
	}

	private int volumeCreditsFor(EnrichedPerformance aPerformance) {
		int result = Math.max(aPerformance.getAudience() - 30, 0);
		if ( aPerformance.play.getType().equals("comedy")) {
			result += Math.floor((double) aPerformance.getAudience() / 5.0);
		}
		return result;
	}

	private Play playFor(Performance perf) {
		Play play = plays.get(perf.getPlayID());
		if (play == null) {
			throw new IllegalArgumentException("No play found");
		}
		return play;
	}

	private int amountFor(EnrichedPerformance aPerformance) {
		int result;
		switch (aPerformance.play.getType()) {
			case "tragedy": result = 40000;
							if (aPerformance.getAudience() > 30) {
								 result += 1000 * (aPerformance.getAudience() - 30);
							}
							break;
			case "comedy":  result = 30000;
							if (aPerformance.getAudience() > 20) {
								result += 10000 + 500 * (aPerformance.getAudience() - 20);
							}
							result += 300 * aPerformance.getAudience();
							break;
			default:        throw new IllegalArgumentException("unknown type: " + aPerformance.play.getType());
		}
		return result;
	}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
